#!/bin/bash

if [[ ! "$(id -u)" -eq 0 ]]; then
    echo "deploy_locally.sh run as non-sudo, aborting."
    exit 1
fi

if [ ! -f ./test.sh ]; then
    echo "deploy_locally.sh not run from its containing directory, aborting."
    exit 1
fi

rsync --recursive --progress joint/root/* /
rsync --recursive --progress software-management-server-only/root/* /

# Manually load /etc/environment in the case that it changed since last run.
#while read -r env; do export "$env"; done
while read environment_variable; do
    export $environment_variable
done < /etc/environment

