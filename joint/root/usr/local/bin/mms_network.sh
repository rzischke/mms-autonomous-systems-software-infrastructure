#!/bin/bash

[ -n "$MMS_NETWORK_GUARD" ] && return || readonly MMS_NETWORK_GUARD=1

source "mms_assert.sh"
source "mms_do.sh"
source "mms_print.sh"

# Global variables containing the domain names of the other computers
# (probably all on the car), which can be defined in /etc/hosts if
# they're not on dns. See here:
# http://man7.org/linux/man-pages/man5/hosts.5.html

readonly JETSON1="jetson1"

readonly HOST_ARRAY=\
( \
    "$JETSON1" \
)

readonly HOST_ARRAY_NVIDIA=\
( \
    "true" \
)

# Capture the user actually running this script, not root
# since it is being run as sudo. The local user root will
# be sshing into the destination is the remote user
# $USER_SUDOING.

readonly USER_SUDOING="$(logname 2>/dev/null || echo ${SUDO_USER:-${USER}})"
# See here https://askubuntu.com/questions/424237/who-w-logname-broken-in-multiple-terminals/849469#849469

# Define an mms_ssh function. This can be changed for all mms_ssh
# calls globally if a different configuration is desired.

function mms_ssh {
    if [ "$MMS_INSECURE_SSH" = true ]; then
        # This branch is used in testing for docker container replicas that
        # cannot be accessed aside from on the hosting computer.
        ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no $@
    else
        ssh $@
    fi
    local ssh_return_value="$?"
    if [[ "$ssh_return_value" -ne 0 ]]; then
        mms_println "Uh oh, something went wrong: ssh returned ${ssh_return_value}."
        exit "$ssh_return_value"
    fi
}

# Disable the password requirements on sudo on the car computers so that
# the runner of the script isn't asked for the password a million times.
# This requirement can be re-enabled at the end of the script.
# See here:
# https://anonscm.debian.org/cgit/collab-maint/sudo.git/plain/debian/README

readonly MMS_SUDOERS_NOPASSWD_FILE="/etc/sudoers.d/${USER_SUDOING}_nopasswd"

function mms_ssh_keygen_at_file {
    mms_assert "$# -eq 1"
    local file="$1"
    mms_println "Generating a new ssh key at ${file}..."
    mms_do "ssh-keygen -f \"${file}\" -t rsa -N ''"
    mms_println "Key generate at ${file}."
}

function mms_ssh_keygen {
    mms_assert "$# -eq 0"
    mms_println "Generating a new ssh key for root..."
    mms_ssh_keygen_at_file "/root/.ssh/id_rsa"
    mms_println "Key generated for root."
}

function mms_ssh_keygen_user_sudoing {
    mms_assert "$# -eq 0"
    mms_println "USER_SUDOING = ${USER_SUDOING}"
    mms_println "Generating a new ssh key for ${USER_SUDOING}..."
    mms_ssh_keygen_at_file "/home/${USER_SUDOING}/.ssh/id_rsa"
    chown "${USER_SUDOING}:${USER_SUDOING}" "/home/${USER_SUDOING}/.ssh/id_rsa"
    mms_println "Key generated for ${USER_SUDOING}."
}

function mms_ssh_copy_id {
    mms_assert "$# -eq 1"
    local host="$1"
    mms_println "Copying ssh public id to ${host}..."
    mms_do "ssh-copy-id -i /root/.ssh/id_rsa.pub ${USER_SUDOING}@${host}"
    # The -i /root/.ssh/id_rsa.pub is because [ssh, ssh-keygen] and ssh-copy-id are inconsistent
    # with regard to the default identity file under sudo. The former default to root, the later
    # defaults to the sudoing user. sshing within sudo also uses root.
    mms_println "Copying complete."
}

function mms_ssh_copy_id_to_all_hosts_once_each {
    mms_println "Copying ssh public id to all hosts..."
    for host in "${HOST_ARRAY[@]}"; do
        mms_do_once "mms_ssh_copy_id $host"
    done
    #mms_do_once "mms_ssh_copy_id $JETSON1"
    mms_println "Ssh public id copied to all hosts."
}

function mms_ssh_disable_password_on_sudo {
    mms_assert "$# -eq 1"
    local host="$1"
    mms_println "Disabling password prompt on sudo for user ${USER_SUDOING}, host ${host}..."
    mms_ssh -tt "${USER_SUDOING}@${host}" "sudo echo \"${USER_SUDOING} ALL=(ALL) NOPASSWD: ALL\" | sudo tee \"${MMS_SUDOERS_NOPASSWD_FILE}\" > /dev/null ; sudo chmod 0440 \"${MMS_SUDOERS_NOPASSWD_FILE}\""
    mms_println "Prompt disabled."
}

function mms_ssh_enable_password_on_sudo {
    mms_assert "$# -eq 1"
    local host="$1"
    mms_println "Enabling password prompt on sudo for user ${USER_SUDOING}, host ${host}..."
    mms_ssh "${USER_SUDOING}@${host}" "sudo rm ${MMS_SUDOERS_NOPASSWD_FILE}"
    # This will error if mms_ssh_disable_password_on_sudo hasn't
    # been run for the passed host, and is the intended behaviour.
    mms_println "Prompt enabled."
}

function mms_ssh_disable_password_on_sudo_all_hosts {
    mms_assert "$# -eq 0"
    mms_println "Disabling password prompt on sudo for user ${USER_SUDOING}, all hosts..."
    mms_ssh_disable_password_on_sudo "$JETSON1"
    mms_println "Sudo password prompt disabled for user ${USER_SUDOING}, all hosts."
}

function mms_ssh_enable_password_on_sudo_all_hosts {
    mms_assert "$# -eq 0"
    mms_println "Enabling password prompt on sudo for user ${USER_SUDOING}, all hosts..."
    for host in "${HOST_ARRAY[@]}"; do
        mms_ssh_enable_password_on_sudo "$host"
    done
    #mms_ssh_enable_password_on_sudo "$JETSON1"
    mms_println "Sudo password prompt enabled for user ${USER_SUDOING}, all hosts."
}

function mms_rsync {
    if [ "$MMS_INSECURE_SSH" = true ]; then
        # This branch is used in testing for docker container replicas that
        # cannot be accessed aside from on the hosting computer.
        mms_do "rsync --rsh=\"ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no\" --rsync-path=\"sudo rsync\" --recursive --progress $@"
    else
        mms_do "rsync --rsh=ssh --rsync-path=\"sudo rsync\" --recursive --progress $@"
    fi
}

