#!/bin/bash
# Inspired by
# http://tldp.org/LDP/abs/html/debugging.html#ASSERT

[ -n "$MMS_ASSERT_GUARD" ] && return || readonly MMS_ASSERT_GUARD=1

source "mms_print.sh"

function mms_assert {
    local e_param_err=98
    local e_assert_failed=99

    if [ "$#" -ge 2 ]; then
        mms_println "Incorrect number of parameters."
        exit $e_param_err
    elif [ "$#" -eq 0 ]; then
        mms_println_base "Assertion failed." "${BASH_SOURCE[1]}" "${FUNCNAME[1]}" "${BASH_LINENO[0]}"
    fi

    if [[ ! $1 ]]; then
        red=$(tput setaf 1)
        no_colour=$(tput sgr0)
        # no colour for now, messes with spacing
        mms_println_base "Assertion failed: \"$1\"" "${BASH_SOURCE[1]}" "${FUNCNAME[1]}" "${BASH_LINENO[0]}"
        exit $e_assert_failed
    fi
}

