#!/bin/bash

MMS_INSECURE_SSH="true"
MMS_OMIT_USER_CREATION="true"

if [[ ! "$(id -u)" -eq 0 ]]; then
    echo "test_install.sh run as non-sudo, aborting."
    exit 1
fi

if [ ! -f ./test_install.sh ]; then
    echo "test_install.sh not run from its containing directory, aborting."
    exit 1
fi

source ./generate_version_header.sh

source ./deploy_locally.sh

source "mms_deploy.sh"
source "mms_execute.sh"
source "mms_network.sh"

mms_deploy_all_hosts

mms_execute_local
mms_execute_remote_all_hosts

