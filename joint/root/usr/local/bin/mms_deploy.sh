#!/bin/bash

[ -n "$MMS_DEPLOY_GUARD" ] && return || readonly MMS_DEPLOY_GUARD=1

source "mms_network.sh"

function mms_deploy_base {
    mms_assert "$# -eq 2"
    local host="$1"
    local root_folder="$2"
    mms_println "Deploying ${root_folder}/* to ${USER_SUDOING}@${host}:/ ..."
    mms_rsync "${root_folder}/*" "${USER_SUDOING}@${host}:/"
    mms_println "Deploy complete."
}

function mms_deploy_joint_scope {
    mms_assert "$# -eq 1"
    local host="$1"
    mms_deploy_base "$host" "joint/root"
}

function mms_deploy_host_scope {
    mms_assert "$# -eq 1"
    local host="$1"
    mms_deploy_base "$host" "${host}-only/root"
}

function mms_deploy_all_hosts {
    mms_assert "$# -eq 0"
    mms_println "Deploying source code, configuration and other assets to all hosts..."
    for host in "${HOST_ARRAY[@]}"; do
                                        # Prioritise local scope:
        mms_deploy_joint_scope "$host"  # do this first, so that
        mms_deploy_host_scope  "$host"  # conflicts are overriden here.
    done
    mms_println "Deploy to all hosts complete."
}

