#!/bin/bash

[ -n "$MMS_EXECUTE_VARIABLES_GUARD" ] && return || readonly MMS_EXECUTE_VARIABLES_GUARD=1

readonly ADMIN_GROUP="mms-admin"
readonly MMS_ASSETS_DIR="/tmp/mms-autonomous-systems-software-infrastructure"
readonly MMS_MACHINE_STATE_DIR="/var/local/mms/${ADMIN_GROUP}"

