#include <iostream>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char **argv) {
    unsigned int t = 20u;
    while (t != 0u) t = sleep(t);
    char *user = getenv("USER");
    std::cout << "Congratulations " << user << "! You've just finished task 6! ";
    std::cout << "Unless you didn't log out, or you didn't email this message. ";
    std::cout << "In that case you're a filthy cheat." << std::endl;
    return EXIT_SUCCESS;
}
