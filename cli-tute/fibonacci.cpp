#include <iostream>
#include <string>

unsigned long slow_fibonacci(unsigned long n) {
    if (n == 0) {
        return 0;
    } else if (n == 1) {
        return 1;
    } else {
        return slow_fibonacci(n-1) + slow_fibonacci(n-2);
    }
}

// unsigned long fast_fibonacci(unsigned long n) {
    // Your code goes here.
// }

int main(int argc, char **argv) {
    char *binary_name = argv[0];

    if (argc != 2) {
        std::cout << binary_name << " calculates the fibonacci number at the provided index." << std::endl;
        std::cout << "Usage: " << binary_name << " <index>" << std::endl;
        return EXIT_FAILURE;
    }

    char *index_string = argv[1];
    unsigned long index = std::stoul(index_string);
    
    std::cout << slow_fibonacci(index) << std::endl;
    //std::cout << fast_fibonacci(index) << std::endl;    // Uncomment this line when your fast_fibonacci function is ready.
    
    return EXIT_SUCCESS;
}
