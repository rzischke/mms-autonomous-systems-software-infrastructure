#!/bin/bash

source "mms_assert.sh"
source "mms_print.sh"

#mms_assert "$# -eq 4"
mms_assert "$# -eq 2"
mms_assert "$(id -u) -eq 0"

USERNAME="$1"
#FIRSTNAME="$2"
#LASTNAME="$3"
EMAIL="$2"

mms_println "Creating user ${USERNAME}, and sending first login details to ${EMAIL}..."

PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 12 | head -n 1)

adduser "$USERNAME" --gecos "" --disabled-password
sudo chpasswd <<< "${USERNAME}:${PASSWORD}"
chage -d 0 "$USERNAME"

IP_ADDR="$(hostname -I)"

printf "Host: ${IP_ADDR}\nUsername: ${USERNAME}\nPassword: ${PASSWORD}\n" | mailx -s "New User at ${IP_ADDR}" "${EMAIL}"

mms_println "Account created, email sent."

