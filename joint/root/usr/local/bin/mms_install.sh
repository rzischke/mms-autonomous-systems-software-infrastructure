#!/bin/bash
# Inspired by
# https://stackoverflow.com/questions/1298066/check-if-a-package-is-installed-and-then-install-it-if-its-not
#
# Because of the many different ways packages are installed if they aren't in the default repositories,
# a directory containing a .sh file for each downloaded package will be used.

[ -n "$MMS_INSTALL_GUARD" ] && return || readonly MMS_INSTALL_GUARD=1

source "mms_do.sh"
source "mms_assert.sh"
source "mms_directories.sh"
source "mms_print.sh"
source "mms_execute.sh"

readonly MMS_PACKAGE_DIR="${MMS_ASSETS_DIR}/packages"

function mms_package_is_installed {
    # Use like this
    # if mms_package_is_installed <package>; then
    #   COMMANDS
    # fi

    mms_assert "$# -eq 1"
    # redirect stderro to /dev/null, we know that the package might not exist.
    local return_value=$(dpkg-query --showformat '${Status}\n' --show "$1" 2>/dev/null | grep -c "ok installed")
    mms_assert "${return_value} -eq 1 || ${return_value} -eq 0"
    if [[ ${return_value} -eq 1 ]]; then
        return 0
    else
        return 1
    fi
}

function mms_install_package {
    mms_assert "$# -eq 1"
    local package_name="$1"
    if mms_package_is_installed "$package_name"; then
        mms_println "${package_name} already installed, no action taken."
    else
        mms_println "Package ${package_name} installing..."
        apt-get --quiet --yes install "$package_name"
        local apt_get_return_value="$?"
        if [[ "$apt_get_return_value" -eq 0 ]]; then
            mms_println "Package ${package_name} installation complete."
        else
            mms_println "Uh oh, something went wrong. Returning return code of failed apt-get install."
            exit "$apt_get_return_value"
        fi
    fi
}

function mms_apt_get_update {
    mms_assert "$# -eq 0"

    mms_println "Updating cache of software repository metadata..."

    mms_do "apt-get update"

    mms_println "Update complete."
}

function mms_install_packages_from_dir {
    mms_assert "$# -eq 1"
    local dir_name="$1"
    local build_dir="${dir_name}/build"

    mms_println "Installing packages in ${dir_name}..."

    mms_install_package "cmake"

    mms_do "chmod --recursive 700 ${dir_name}"  # Make scripts executable.
    mms_create_dir "${build_dir}" "root:root" "700"

    # Enclose in set (-/+)e, so that errors in package installation crash
    # the whole installation.

    set -e
    ( cd "${build_dir}" && cmake .. )
    ( cd "${build_dir}" && make )
    set +e
    mms_do "rm -r ${build_dir}"

    mms_println "Package installation complete."
}

