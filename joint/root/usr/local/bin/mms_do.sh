#!/bin/bash

[ -n "$MMS_ONCE_GUARD" ] && return || readonly MMS_ONCE_GUARD=1

source "mms_print.sh"
source "mms_execute_variables.sh"

#readonly MMS_ONCE_DIRECTORY="/var/local/mms/once"
# change this to /var/local/mms/admin/once
readonly MMS_ONCE_DIRECTORY="${MMS_MACHINE_STATE_DIR}/once"

if [ ! -d "$MMS_ONCE_DIRECTORY" ]; then
    mkdir --parents "$MMS_ONCE_DIRECTORY"
fi

function mms_do {
    eval "$@"
    local return_value="$?"
    if [[ ! "$return_value" -eq 0 ]]; then
        mms_println "Something went wrong: \"$@\" returned ${return_value}."
        exit "$return_value"
    fi
}

function mms_do_once {
    local cmd_md5sum=$(echo -n "$@" | md5sum | awk '{print $1}')
    local done_file="${MMS_ONCE_DIRECTORY}/${cmd_md5sum}"
    if [ ! -f "$done_file" ]; then
        mms_do "$@"
        touch "$done_file"
    fi
}

function mms_do_again_later {
    local cmd_md5sum=$(echo -n "$@" | md5sum | awk '{print $1}')
    local done_file="${MMS_ONCE_DIRECTORY}/${cmd_md5sum}"
    if [ -f "$done_file" ]; then
        rm "$done_file"
        mms_println "Done file ${done_file} for command \"$@\" was deleted."
    else
        mms_println "Done file ${done_file} for command \"$@\" did not exist."
    fi
}

