#!/bin/bash

[ -n "$MMS_DIRECTORIES_GUARD" ] && return || readonly MMS_DIRECTORIES_GUARD=1

source "mms_do.sh"
source "mms_assert.sh"
source "mms_print.sh"
source "mms_execute.sh"
source "mms_account.sh"

readonly MMS_GLOBAL_DIR_FILE="${MMS_ASSETS_DIR}/directories/directories-global"
readonly MMS_LOCAL_DIR_FILE="${MMS_ASSETS_DIR}/directories/directories-local"

function mms_dir_has_group {
    # Use like this
    # if mms_dir_has_group <dir>; then
    #   COMMANDS
    # fi
    mms_assert "$# -eq 1"

    local dir="$1"
    if [[ $dir =~ .*\$\{?group\}?.* ]]; then
        return 0
    else
        return 1
    fi
}

function mms_create_dir {
    mms_assert "$# -geq 3"

    local args="$@"
    local location="$1"
    local ownership="$2"
    local permissions="$3"

    mms_println "Applying directory ${args}..."

    # --parents flag needed not to error if dir already exists.
    mms_do "mkdir --parents ${location}"
    mms_do "chown ${ownership} ${location}"
    mms_do "chmod ${permissions} ${location}"
    
    local i=1
    for arg in $args; do
        if [[ $i -ge 4 ]]; then
            mms_do "setfacl --modify ${arg} ${location}"
        fi
        let "i+=1"
    done

    mms_println "Done."
}

function mms_process_dir_file_line {
    mms_assert "$# -geq 4"

    local group_file="$1"
    shift
    # $1 is removed, what was in $n is now in $n-1

    local args="$@"

    if mms_dir_has_group "$@"; then
        while IFS= read -r group_in; do
            if [ "$group_in" ]; then
                local group="$group_in"
                # The group variable is referenced in the location string.

                eval local args_expanded="\"${args}\""
                # Eval needed to expand $group or ${group} in arguments.

                mms_create_dir $args_expanded
            fi
        done < "$group_file"
    else
        mms_create_dir $args
    fi
}

function mms_apply_dir_file {
    mms_assert "$# -eq 2"

    local dir_file="$1"
    local group_file="$2"

    mms_println "Applying directories in ${dir_file} with groups in ${group_file}..."

    while IFS= read -r dir_file_line; do
        if [ "$dir_file_line" ]; then
            mms_process_dir_file_line "$group_file" $dir_file_line
        fi
    done < "$dir_file"

    mms_println "All directories applied."
}

