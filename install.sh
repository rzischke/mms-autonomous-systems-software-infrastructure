#!/bin/bash

if [[ ! "$(id -u)" -eq 0 ]]; then
    echo "install.sh run as non-sudo, aborting."
    exit 1
fi

if [ ! -f ./install.sh ]; then
    echo "install.sh not run from its containing directory, aborting."
    exit 1
fi

source ./generate_version_header.sh

source ./deploy_locally.sh

source "mms_deploy.sh"
source "mms_execute.sh"
source "mms_network.sh"

mms_do_once mms_ssh_keygen

mms_ssh_copy_id_to_all_hosts_once_each

mms_ssh_disable_password_on_sudo_all_hosts

mms_deploy_all_hosts

mms_execute_local
mms_execute_remote_all_hosts

# mms_ssh_enable_password_on_sudo_all_hosts
# This is commented out so that after the first
# run, continuous integration can work automatically,
# without human intervention.

