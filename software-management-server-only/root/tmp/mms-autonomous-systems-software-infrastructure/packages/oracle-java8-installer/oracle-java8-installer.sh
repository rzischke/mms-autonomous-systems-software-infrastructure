#!/bin/bash

source "mms_install.sh"

# oracle-java8-installer is broken (18/07/2018), so the
# installer will be manually patched here after the repository
# is added. This script may break in the future.

# See here:
# https://stackoverflow.com/questions/46815897/jdk-8-is-not-installed-error-404-not-found

# Auto agree to terms and conditions.
debconf-set-selections <<< "oracle-java8-installer oracle-java8-installer/local string"
debconf-set-selections <<< "oracle-java8-installer shared/accepted-oracle-license-v1-1 boolean true"
debconf-set-selections <<< "oracle-java8-installer shared/present-oracle-license-v1-1 note"

mms_println "Package oracle-java8-installer is broken, and unfortunately takes a long time to install. First, the apt-key needs to be added, an apt-get update needs to be done, the package needs to be installed (and fail) so that this script can edit the dpkg installation files, and then the package needs to be downloaded again, later, with all of the other packages."

set +e
apt-get --quiet --yes install oracle-java8-installer &> /dev/null
set -e

dpkg_info="/var/lib/dpkg/info"

sed -i 's|JAVA_VERSION=8u171|JAVA_VERSION=8u181|' ${dpkg_info}/oracle-java8-installer.*
sed -i 's|PARTNER_URL=http://download.oracle.com/otn-pub/java/jdk/8u171-b11/512cd62ec5174c3487ac17c61aaa89e8/|PARTNER_URL=http://download.oracle.com/otn-pub/java/jdk/8u181-b13/96a7b8442fe848ef90c96a2fad6ed6d1/|' ${dpkg_info}/oracle-java8-installer.*
sed -i 's|SHA256SUM_TGZ="b6dd2837efaaec4109b36cfbb94a774db100029f98b0d78be68c27bec0275982"|SHA256SUM_TGZ="1845567095bfbfebd42ed0d09397939796d05456290fb20a83c476ba09f991d3"|' ${dpkg_info}/oracle-java8-installer.*
sed -i 's|J_DIR=jdk1.8.0_171|J_DIR=jdk1.8.0_181|' ${dpkg_info}/oracle-java8-installer.*

mms_install_package "oracle-java8-installer"

