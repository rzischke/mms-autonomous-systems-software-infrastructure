#!/bin/bash

source "mms_assert.sh"
source "mms_print.sh"

#function mms_file_maze_base {
#    mms_assert "$# -eq 3"
#    local source_directory="$1"
#    local directories_per_layer="$2"
#    local number_of_layers="$3"
#    if [[ "$number_of_layers" -eq 0 ]]; then
#        return
#    else
#        let next_number_of_layers=number_of_layers-1
#        local directory_countdown=$directories_per_layer
#        while [ $directory_countdown -ne 0 ]; do
#            local dirname=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 12 | head -n 1)
#            local full_dirname="${source_directory}/${dirname}"
#            echo "${full_dirname}"
#            mkdir "${full_dirname}"
#            mms_file_maze_base "${full_dirname}" "${directories_per_layer}" "${next_number_of_layers}"
#            let directory_countdown=directory_countdown-1
#        done
#    fi
#}

function mms_file_maze {
    mms_assert "$# -ge 3"
    local source_directory="$1"
    local directories_per_layer="$2"
    local number_of_layers="$3"
    local total_number_of_directories=$(($directories_per_layer ** $number_of_layers))
    mms_println "Creating file maze begining at ${source_directory} of ${number_of_layers} layers with ${directories_per_layer} directories per layer, for a total of ${total_number_of_directories} directories..."
    ./mms_file_maze_base $@
    mms_println "Done."
}

