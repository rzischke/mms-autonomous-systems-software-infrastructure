#!/bin/bash

source "mms_assert.sh"

mms_assert "$# -eq 1"
mms_assert "$(id -u) -eq 0"

ACCOUNTS_FILE_NAME="$1"

while read line; do
    username=$(echo -n "$line" | cut --delimiter=' ' --fields=1)
    userdel -r $username
done < "$ACCOUNTS_FILE_NAME"

rm /usr/local/bin/twenty

