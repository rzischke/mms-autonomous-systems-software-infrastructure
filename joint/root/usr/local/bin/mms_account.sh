#!/bin/bash

[ -n "$MMS_ACCOUNT_GUARD" ] && return || readonly MMS_ACCOUNT_GUARD=1

source "mms_assert.sh"
source "mms_print.sh"
source "mms_execute.sh"

readonly MMS_ACCOUNT_DIRECTORY="${MMS_MACHINE_STATE_DIR}/account"
readonly MMS_USER_DIRECTORY="${MMS_ACCOUNT_DIRECTORY}/user"
readonly MMS_GROUP_DIRECTORY="${MMS_ACCOUNT_DIRECTORY}/group"
readonly MMS_USER_FILE="${MMS_ASSETS_DIR}/account/users"
readonly MMS_GROUP_FILE="${MMS_ASSETS_DIR}/account/groups"
readonly LIST_USERS_CMD="getent passwd | cut -d: -f1"
readonly LIST_GROUPS_CMD="getent group | cut -d: -f1"

if [ ! -d "$MMS_ACCOUNT_DIRECTORY" ]; then
    mkdir "$MMS_ACCOUNT_DIRECTORY"
fi

if [ ! -d "$MMS_USER_DIRECTORY" ]; then
    mkdir "$MMS_USER_DIRECTORY"
fi

if [ ! -d "$MMS_GROUP_DIRECTORY" ]; then
    mkdir "$MMS_GROUP_DIRECTORY"
fi

function mms_get_users_dir {
    # Echos a user's directory, which stores the information for mms managed
    # users.
    # users_dir_var=$(mms_get_users_dir <user>)
    
    mms_assert "$# -eq 1"

    local username="$1"
    local users_dir="${MMS_USER_DIRECTORY}/${username}"

    # The creation of this directory is managed by the mms_new_user function.
    # This is so that attempting to add a new user that already exists and is
    # not managed by mms can result in an abort.
    #if [ ! -d "$users_dir" ]; then
    #    mkdir "$users_dir"
    #fi

    printf "$users_dir"
}

function mms_get_users_groups_dir {
    # Echos a user's groups directory, which stores the mms managed
    # groups they are in.
    # groups_dir_var=$(mms_get_users_groups_dir <user>)

    mms_assert "$# -eq 1"    
    
    local username="$1"
    local users_dir="$(mms_get_users_dir "$username")"
    local users_groups_dir="${users_dir}/groups"

    if [ ! -d "$users_groups_dir" ]; then
        mkdir "$users_groups_dir"
    fi    

    printf "$users_groups_dir"
}

function mms_user_exists {
    # Use like this
    # if mms_user_exists <user> <whateverelse>...; then
    #   COMMANDS
    # fi
    mms_assert "$# -geq 1"

    # redirect stderr to /dev/null, we know that the user/group might not exist.
    id "-u" "$1" &> /dev/null
    return "$?"
}

function mms_group_exists {
    # Use like this
    # if mms_group_exists <group>; then
    #    COMMANDS
    # fi
    mms_assert "$# -eq 1"

    # redirect stderr to /dev/null, we know that the user/group might not exist.
    getent group "$1" &> /dev/null
    return "$?"
}

function mms_user_in_group {
    # Use like this
    # if mms_user_in_group <user> <group>; then
    #   <commands for user is in group>
    # else
    #   <commands for user is not in group>
    # fi
    mms_assert "$# -eq 2"
    local username="$1"
    local groupname="$2"
    if id -nG "$username" | grep -qw "$groupname"; then
        return 0;
    else
        return 1;
    fi
}

function mms_user_is_registered {
    # Use like this
    # if mms_user_is_registered <user>; then
    #   <commands for user is registered>
    # else
    #   <commands for user is not registered>
    # fi
    mms_assert "$# -geq 1"
    local username="$1"
    if ls "$(mms_get_users_dir "$username")" &> /dev/null; then
        return 0;
    else
        return 1;
    fi
}

function mms_group_is_registered {
    # Use like this
    # if mms_group_is_registered <group>; then
    #   <commands for group is registered>
    # else
    #   <commands for group is not registered>
    # fi
    mms_assert "$# -eq 1"
    local groupname="$1"
    if ls "${MMS_GROUP_DIRECTORY}/${groupname}" &> /dev/null; then
        return 0;
    else
        return 1;
    fi
}

function mms_validate_user_group_characters {
    # Validates that any user or group matches a conservative regexp.
    # This should reject any acceidental trailing whitespace.
    
    mms_assert "$# -eq 1"

    local name="$1"
    if [[ ! "$name" =~ ^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$ ]]; then
        mms_println "User or group name \"${name}\" did not match validation regexp."
        mms_assert
    fi
}
    

function mms_validate_user {
    # Validates that all of the groups for the user to belong to exist.

    mms_assert "$# -geq 2"

    local user="$1"
    local users_dir="$(mms_get_users_dir "$user")"

    mms_validate_user_group_characters "$user"

    if mms_user_exists "$user"; then
        if [ ! -d "$users_dir" ]; then
            mms_print "Tried to add an unrecognised user ($user) that already exists."
            mms_assert
        fi
    else
        if [ -d "$users_dir" ]; then
            mms_print "The user ${user} does not exist but is registered at ${users_dir}."
            mms_assert
        fi
    fi

    local i=1
    for arg in "$@"; do
        if [[ $i -ge 3 ]]; then
            if ! mms_group_exists "$arg"; then
                mms_println "Tried to add user \"${user}\" to group \"${arg}\", which doesn't exist."
                mms_assert
            fi
        fi
        let "i+=1"
    done

    mms_println "User $user validated."
}

function mms_validate_group {
    mms_assert "$# -eq 1"

    local group="$1"
    local group_file="${MMS_GROUP_DIRECTORY}/${group}"

    mms_validate_user_group_characters "$group"

    if mms_group_exists "$group"; then
        if [ ! -f "$group_file" ]; then
            cat "$group_file"
            mms_println "Tried to add an unregistered group ($group) that already exists."
            mms_assert
        fi
    else
        if [ -f "$group_file" ]; then
            mms_print "The group ${group} does not exist but it's registration file ${group_file} exists."
            mms_assert
        fi
    fi

    mms_println "Group $group validated."
}

function mms_new_user_email {
    mms_assert "$# -eq 3"

    local username="$1"
    local email="$2"
    local password="$3"
    local hostname_var="$(hostname)"
    local ip_var="$(hostname -i)"
    printf "Host: ${hostname_var}\nIP: ${ip_var}\nUsername: ${username}\nPassword: ${password}\n" | mailx -s "New User at ${hostname_var}" "${email}"
    mms_println "New user email sent to ${email} for user ${username}, since MMS_SEND_EMAILS == ${MMS_SEND_EMAILS}."    
}

function mms_add_user_to_group {
    # Adds a user to a group.
    # mms_add_user_to_group <user> <group>
    
    local username="$1"
    local groupname="$2"
    local users_groups_dir="$(mms_get_users_groups_dir "$username")"
    local users_group_file="${users_groups_dir}/${groupname}"

    adduser "$username" "$groupname"
    touch "$users_group_file"

    mms_println "User ${username} added to group ${groupname}."
}

function mms_remove_user_from_group {
    # Removes a user from a group.
    # mms_remove_user_from_group <user> <group>

    local username="$1"
    local groupname="$2"
    local users_groups_dir="$(mms_get_users_groups_dir "$username")"
    local users_group_file="${users_groups_dir}/${groupname}"

    if [ ! -f ${users_group_file} ]; then
        mms_println "Group file ${users_group_file} for user ${username} doesn't exit."
        mms_abort
    fi

    deluser "$username" "$groupname"
    rm "$users_group_file"

    mms_println "User ${username} removed from group ${groupname}."
}

function mms_new_user {
    # Adds a new user.
    # mms_new_user <username> <email> [<group1> [<group2>...]]

    mms_assert "$# -geq 2"

    local username="$1"
    local email="$2"
    local password=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1)

    adduser "$username" --gecos "" --disabled-password
    chpasswd <<< "${username}:${password}"
    chage -d 0 "$username"  # Force password change on login.

    local users_dir="$(mms_get_users_dir "$username")"
    mkdir "$users_dir"

    mms_println "New user added: ${username}"

    local i=1
    for arg in "$@"; do
        if [[ $i -ge 3 ]]; then
            local groupname="$arg"
            mms_add_user_to_group "$username" "$groupname"
        fi
        let "i+=1"
    done

    mms_new_user_email "$username" "$email" "$password"
}

function mms_update_user {
    # Updates the group membership of an existing user if required.
    # Otherwise takes no action.
    # mms_update_user <username> <email> [<group1> [<group2>...]]

    mms_assert "$# -geq 2"

    local username="$1"
    local users_groups_dir="$(mms_get_users_groups_dir "$username")"

    # Add the user to groups it should be in but isn't.
    local i=1
    for arg in "$@"; do
        if [ $i -ge 3 ]; then
            local groupname="$arg"
            if ! mms_user_in_group "$username" "$groupname"; then
                mms_add_user_to_group "$username" "$groupname"
            fi
        fi
        let "i+=1"
    done

    # Remove the user from any groups it shouldn't be in but is.
    for group_file_path in ${users_groups_dir}/*; do

        groupname_is="$(basename "$group_file_path")"

        if [ "${groupname_is}" == "*" ]; then
            break
        fi

        if ! mms_user_in_group "$username" "$groupname_is"; then
            mms_println "User ${username} not in group registered in ${users_groups_dir}"
            mms_assert
        fi

        i=1
        local remove_from_group=1   # Set to "yes"
        for arg in "$@"; do
            if [ $i -ge 3 ]; then
                local groupname_okay="$arg"
                if [ "$groupname_is" == "$groupname_okay" ]; then
                    remove_from_group=0    # Set to "no"
                fi
            fi
            let "i+=1"
        done

        if [ $remove_from_group -eq 1 ]; then
            mms_remove_user_from_group "$username" "$groupname_is"
        fi
    done
}

function mms_remove_user {
    # mms_remove_user <user> <whateverelse>...
    
    mms_assert "$# -geq 1"

    local username="$1"
    local users_dir="$(mms_get_users_dir "$username")"

    deluser "$username"
    rm -r "$users_dir"

    mms_println "User ${username} removed."
}

function mms_new_group {
    # mms_new_group <groupname>

    mms_assert "$# -eq 1"

    local groupname="$1"
    local group_file="${MMS_GROUP_DIRECTORY}/${groupname}"

    addgroup "$groupname"
    touch "$group_file"

    mms_println "New group: $groupname"
}

function mms_remove_group {
    # mms_remove_group <groupname>

    mms_assert "$# -eq 1"

    local groupname="$1"
    local group_file="${MMS_GROUP_DIRECTORY}/${groupname}"

    delgroup "$groupname"
    rm "$group_file"

    mms_println "Group ${groupname} removed."
}

function mms_apply_users_and_group_file {
    mms_assert "$# -eq 2"

    local users_file="$1"
    local group_file="$2"

    # See here for loop
    # structure: 
    # http://mywiki.wooledge.org/BashFAQ/001

    # Validate group list.
    while IFS= read -r group_in; do
        if [ "$group_in" ]; then
            mms_validate_group "$group_in"
        fi
    done < "$group_file"

    # Add new groups.
    while IFS= read -r group_in; do
        if [ "$group_in" ]; then
            if ! mms_group_exists "$group_in"; then
                mms_new_group "$group_in"
            fi
        fi
    done < "$group_file"

    # Validate user list.
    while IFS= read -r user_line; do
        if [ "$user_line" ]; then
            mms_validate_user $user_line
        fi
    done < "$users_file"

    # Skip registering of users if MMS_OMIT_USER_CREATION
    # is true
    if [ ! "$MMS_OMIT_USER_CREATION" = true ]; then

        # Add new users, and update existing ones.
        while IFS= read -r user_line; do
            if [ "$user_line" ]; then
                if mms_user_exists $user_line; then
                    mms_update_user $user_line
                else
                    mms_new_user $user_line
                fi
            fi
        done < "$users_file"

        # Delete registered users that aren't in the list any more.
        for username_reg_path in "${MMS_USER_DIRECTORY}"/*; do

            local username_reg="$(basename "${username_reg_path}")"

            if [ "${username_reg}" == "*" ]; then
                break
            fi

            local remove_user=1;    # Set to "yes"
            while IFS= read -r user_line; do
                if [ "$user_line" ]; then
                    set -- $user_line
                    local username_from_list="$1"
                    if [ "$username_reg" == "$username_from_list" ]; then
                        remove_user=0;  # Set to "no"
                    fi
                fi
            done < "$users_file"
            if [[ "$remove_user" -eq 1 ]]; then
                mms_remove_user "${username_reg}"
            fi
        done

    fi

    # Delete registered groups that aren't in the list any more.

    for group_reg_path in "${MMS_GROUP_DIRECTORY}"/*; do

        local group_reg="$(basename "${group_reg_path}")"

        if [ "${group_reg}" == "*" ]; then
            break
        fi

        local remove_group=1;   # Set to "yes"
        while IFS= read -r group_from_list; do
            if [ "$group_from_list" ]; then
                if [ "$group_reg" == "$group_from_list" ]; then
                    remove_group=0; # Set to "no"
                fi
            fi
        done < "$group_file"
        if [[ "$remove_group" -eq 1 ]]; then
            mms_remove_group "${group_reg}"
        fi
    done
}

