#!/bin/bash

if [[ ! "$(id -u)" -eq 0 ]]; then
    echo "test.sh run as non-sudo, aborting."
    exit 1
fi

if [ ! -f ./test.sh ]; then
    echo "test.sh not run from its containing directory, aborting."
    exit 1
fi

source ./generate_version_header.sh

source ./deploy_locally.sh

MMS_INSECURE_SSH=" "    # mms_ssh now does not check host file.
source "mms_do.sh"
source "mms_network.sh"

TMP="./test-autogen"
mkdir --parents "${TMP}"

if [ ! -f /root/.ssh/id_rsa.pub ]; then
    mms_ssh_keygen
fi

cat "/root/.ssh/id_rsa.pub" > "${TMP}/authorized_keys"
cat "/home/${USER_SUDOING}/.ssh/id_rsa.pub" >> "${TMP}/authorized_keys"
ssh-keygen -t rsa -f "${TMP}/sms_${USER_SUDOING}_id_rsa"
cat "${TMP}/sms_${USER_SUDOING}_id_rsa.pub" >> "${TMP}/authorized_keys"
ssh-keygen -t rsa -f "${TMP}/sms_root_id_rsa"
cat "${TMP}/sms_root_id_rsa.pub" >> "${TMP}/authorized_keys"
for host in "${HOST_ARRAY[@]}"; do
    ssh-keygen -t rsa -f "${TMP}/${host}_${USER_SUDOING}_id_rsa"
    cat "${TMP}/${host}_${USER_SUDOING}_id_rsa.pub" >> "${TMP}/authorized_keys"
    ssh-keygen -t rsa -f "${TMP}/${host}_root_id_rsa"
    cat "${TMP}/${host}_root_id_rsa.pub" >> "${TMP}/authorized_keys"
done

docker build \
    --build-arg user="${USER_SUDOING}" \
    --build-arg ssh_user_key_priv="${TMP}/sms_${USER_SUDOING}_id_rsa" \
    --build-arg ssh_user_key_pub="${TMP}/sms_${USER_SUDOING}_id_rsa.pub" \
    --build-arg ssh_root_key_priv="${TMP}/sms_root_id_rsa" \
    --build-arg ssh_root_key_pub="${TMP}/sms_root_id_rsa.pub" \
    --build-arg authorized_keys="${TMP}/authorized_keys" \
    --build-arg etc_hosts="${TMP}/etc_hosts" \
    --build-arg sudo_nopasswd_file="${MMS_SUDOERS_NOPASSWD_FILE}" \
    -t mms/sms-oneshot \
    -f test-image-dockerfile \
    .
for host in "${HOST_ARRAY[@]}"; do
    docker build \
        --build-arg user="${USER_SUDOING}" \
        --build-arg ssh_user_key_priv="${TMP}/${host}_${USER_SUDOING}_id_rsa" \
        --build-arg ssh_user_key_pub="${TMP}/${host}_${USER_SUDOING}_id_rsa.pub" \
        --build-arg ssh_root_key_priv="${TMP}/${host}_root_id_rsa" \
        --build-arg ssh_root_key_pub="${TMP}/${host}_root_id_rsa.pub" \
        --build-arg authorized_keys="${TMP}/authorized_keys" \
        --build-arg etc_hosts="${TMP}/etc_hosts" \
        --build-arg sudo_nopasswd_file="${MMS_SUDOERS_NOPASSWD_FILE}" \
        -t "mms/${host}-oneshot" \
        -f test-image-dockerfile \
        .
done

docker network create \
    --driver=bridge \
    --subnet=172.16.0.0/16 \
    --ip-range=172.16.0.0/17 \
    testnet

HOSTS_DOCKER_ARGS="--add-host=sms:172.16.255.1 "
ip="2"
for host in "${HOST_ARRAY[@]}"; do
    HOSTS_DOCKER_ARGS="${HOST_DOCKER_ARGS} --add-host=${host}:172.16.255.${ip} "
    ip=$[$i+1]
done

NVIDIA_DEVICES="$(ls /dev | grep nvidia)"
NVIDIA_DEVICES_DOCKER_ARGS=""
while read -r device; do
    NVIDIA_DEVICES_DOCKER_ARGS="${NVIDIA_DEVICES_DOCKER_ARGS}--device /dev/${device}:/dev/${device} "
done <<< "$NVIDIA_DEVICES"

# Start the containers, passing through NVIDIA devices
# for containers representing hosts that have a gpu.
docker run -d \
    --rm \
    --name="sms-oneshot" \
    -h "sms" \
    --network="testnet" \
    --ip="172.16.255.1" \
    --volume="$(pwd)/:/home/${USER_SUDOING}/repo" \
    ${HOSTS_DOCKER_ARGS} \
    ${NVIDIA_DEVICES_DOCKER_ARGS} \
    mms/sms-oneshot
for ((i = 0 ; i < ${#HOST_ARRAY[@]} ; i++)); do
    ip=$[$i+2]
    host="${HOST_ARRAY[$i]}"
    nvidia_passthrough="${HOST_ARRAY_NVIDIA[$i]}"

    DOCKER_RUN_ARGS="-d --rm --name=${host}-oneshot -h ${host} --network=testnet --ip=172.16.255.${ip} ${HOSTS_DOCKER_ARGS} "
    if $nvidia_passthrough; then
        DOCKER_RUN_ARGS="${DOCKER_RUN_ARGS} ${NVIDIA_DEVICES_DOCKER_ARGS} "
    fi
    DOCKER_RUN_ARGS="${DOCKER_RUN_ARGS} mms/${host}-oneshot"

    docker run ${DOCKER_RUN_ARGS}
done

sleep 5

mms_ssh -tt "${USER_SUDOING}@172.16.255.1" "cd repo && sudo ./test_install.sh"

docker stop sms-oneshot
for host in "${HOST_ARRAY[@]}"; do
    docker stop ${host}-oneshot
done

docker image rm sms-oneshot
for host in "${HOST_ARRAY[@]}"; do
    docker image rm ${host}-oneshot
done

docker network rm testnet

