#!/bin/bash

source "mms_assert.sh"
source "mms_print.sh"
source "./mms_file_maze.sh"

mms_assert "$# -eq 1"
mms_assert "$(id -u) -eq 0"

ACCOUNTS_FILE_NAME="$1"

g++ -O3 mms_file_maze_base.cpp -o mms_file_maze_base
chmod 755 ./mms_file_maze_base

g++ -O3 twenty.cpp -o twenty

mms_println "Creating users from accounts list at ${ACCOUNTS_FILE_NAME}..."

while read line; do
    username=$(echo -n "$line" | cut --delimiter=' ' --fields=1)
    email=$(echo -n "$line" | cut --delimiter=' ' --fields=2)
    homedir="/home/${username}"
    mms_new_user.sh "$username" "$email"
    mkdir "${homedir}/maze"
    mms_file_maze "${homedir}/maze" 2 13 ./challanges.pdf
    chmod --recursive 000 "${homedir}/maze"
    cp fibonacci.cpp ${homedir}
    chown --recursive "${username}:${username}" "$homedir"
done < "$ACCOUNTS_FILE_NAME"

rsync ./twenty /usr/local/bin
chmod 711 /usr/local/bin/twenty

