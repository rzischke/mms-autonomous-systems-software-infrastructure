#!/bin/bash

rosdep init
rosdep update

ros_shell_setup="/etc/profile.d/ros-lunar-setup.sh"
echo "#!/bin/bash" > "${ros_shell_setup}"
echo "source /opt/ros/lunar/setup.bash" >> "${ros_shell_setup}"

