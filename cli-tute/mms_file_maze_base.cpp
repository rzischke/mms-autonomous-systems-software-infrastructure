#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits>
#include <random>
#include <string>

using std::cout;
using std::default_random_engine;
using std::endl;
using std::numeric_limits;
using std::stoi;
using std::string;
using std::to_string;
using std::uniform_int_distribution;

struct stat st = {0};
int directories_per_layer;
default_random_engine generator;


int mms_file_maze(string source_directory, int number_of_layers) {
    if (number_of_layers > 0) {
        for (int i = 0; i != directories_per_layer; ++i) {
            string new_directory = source_directory;
            new_directory += '/';
            new_directory += to_string(i);
            if (stat(new_directory.c_str(), &st) == -1) {
                if (mkdir(new_directory.c_str(), 0700) == 0) {
                    int ret = mms_file_maze(new_directory, number_of_layers-1);
                    if (ret != 0) return ret;
                } else {
                    return 2;
                }
            } else {
                return 1;
            }
        }
    } else if (number_of_layers < 0) {
        return 3;
    }
    return 0;
}

int insert_file_into_maze(char *filename, string source_directory, int number_of_layers) {
    int highest_dest = 1;
    for (int i = 0; i != number_of_layers; ++i) highest_dest *= directories_per_layer;
    highest_dest -= 1;
    //cout << "highest_dest = " << highest_dest << endl;
    uniform_int_distribution<int> dist(0, highest_dest);
    int destination = dist(generator);
    destination = dist(generator);
    //cout << "destination = " << destination << endl;
    string dest_dir = source_directory;
    int depth = 0;
    for (int next = destination; next != 0; next /= directories_per_layer) {
        ++depth;
        //cout << "next = " << next << endl;
        //cout << "chosen_dir = " << (next % directories_per_layer) << endl;
        dest_dir += '/';
        dest_dir += to_string(next % directories_per_layer);
    }
    while (depth != number_of_layers) {
        dest_dir += "/0";
        ++depth;
    }
    //cout << "dest_dir = " << dest_dir << endl;
    string command = "cp ";
    command += filename;
    command += ' ';
    command += dest_dir;
    command += '\n';
    //cout << "command = " << command;
    return system(command.c_str());
}

int main(int argc, char **argv) {
    if (argc > 4) {
        string source_directory(argv[1]);
        directories_per_layer = stoi(string(argv[2]));
        int number_of_layers = stoi(string(argv[3]));
        int ret0 = mms_file_maze(source_directory, number_of_layers);
        if (ret0 != 0) return ret0;
        auto seed = static_cast<unsigned int>(time(NULL) % numeric_limits<unsigned int>::max());
        //cout << "seed = " << seed << endl;
        generator = default_random_engine(seed);
        for (int i = 4; i != argc; ++i) {
            int ret1 = insert_file_into_maze(argv[i], source_directory, number_of_layers);
            if (ret1 != 0) return ret1;
        }
        return 0;
    } else {
        std::cout << "Usage: <cmd> <source_dir> <dirs_per_layer> <number_of_layers> <files>..." << std::endl;
        return 4;
    }
}
