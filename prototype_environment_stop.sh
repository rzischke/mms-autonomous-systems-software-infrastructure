#!/bin/bash

if [[ ! "$(id -u)" -eq 0 ]]; then
    echo "Script run as non-sudo, aborting."
    exit 1
fi

if [ ! -f ./enforce_sudo_wd.sh ]; then
    echo "Script not run in containing directory, aborting."
    exit 1
fi

source ./generate_version_header.sh

source ./deploy_locally.sh

source "mms_test.sh"

stop_docker_replica_environment prototype

