#!/bin/bash

[ -n "$MMS_PRINT_GUARD" ] && return || readonly MMS_PRINT_GUARD=1

function mms_println_base {
    local timestamp_message_gap=1
    local message_debug_gap=2

    local message="$1"
    local source_file="$(basename "$2")"
    local function_name="$3"
    local line_number="$4"

    local date_string=$(date +"%T")
    local timestamp="[${date_string}]"
    local debug_string=""
    
    if [ "$#" -eq "4" ] && [[ ! -z "$source_file" ]] && [[ ! -z "$function_name" ]] && [[ ! -z "$line_number" ]]; then
        debug_string="${source_file}(${line_number}):${function_name}"
    fi

    local terminal_width=$(tput cols)
    local message_row_width=""

    let "message_row_width = ${terminal_width} - ${#debug_string} - ${#timestamp} - ${timestamp_message_gap} - ${message_debug_gap}"
    local message_folded_aligned_left="$(fold -sw $message_row_width <<< "$message")"
    local message_and_timestamp_folded="${timestamp} $(sed ':a;N;$!ba;s/\n/\n           /g' <<< "$message_folded_aligned_left")"
    local final_line="$(tail -n 1 <<< "$message_and_timestamp_folded")"

    local debug_width=""
    let "debug_width = ${terminal_width} - ${#final_line} - 1"
    
    printf "${message_and_timestamp_folded} %${debug_width}s\n" "$debug_string"
}

function mms_println {
    mms_println_base "$@" "${BASH_SOURCE[1]}" "${FUNCNAME[1]}" "${BASH_LINENO[0]}"
}
