#!/bin/bash

MMS_INSECURE_SSH="true"
source "mms_do.sh"
source "mms_network.sh"
source "mms_print.sh"
source "mms_version.sh"

TMP_DIR="./mms-replica-autogen"

function create_docker_replica_environment {
    local dockerfile="$1"
    shift
    local image_suffix="$1"
    shift
    local ip_third_byte="$1"
    shift
    local docker_build_dir="$1"
    shift
    local custom_docker_run_flags="$@"

    mkdir --parents "${TMP_DIR}"
    chown root:root "${TMP_DIR}"
    chmod 700 "${TMP_DIR}"

    if [ ! -f /root/.ssh/id_rsa.pub ]; then
        mms_ssh_keygen
    fi
    cat "/root/.ssh/id_rsa.pub" > "${TMP_DIR}/authorized_keys"

    if [ ! -f "/home/${USER_SUDOING}/.ssh/id_rsa.pub" ]; then
        mms_ssh_keygen_user_sudoing
    fi
    cat "/home/${USER_SUDOING}/.ssh/id_rsa.pub" >> "${TMP_DIR}/authorized_keys"

    mms_ssh_keygen_at_file "${TMP_DIR}/sms_root_id_rsa"
    cat "${TMP_DIR}/sms_root_id_rsa.pub" >> "${TMP_DIR}/authorized_keys"
    mms_ssh_keygen_at_file "${TMP_DIR}/sms_${USER_SUDOING}_id_rsa"
    cat "${TMP_DIR}/sms_${USER_SUDOING}_id_rsa.pub" >> "${TMP_DIR}/authorized_keys"
    for host in "${HOST_ARRAY[@]}"; do
        mms_ssh_keygen_at_file "${TMP_DIR}/${host}_root_id_rsa"
        cat "${TMP_DIR}/${host}_root_id_rsa.pub" >> "${TMP_DIR}/authorized_keys"
        mms_ssh_keygen_at_file "${TMP_DIR}/${host}_${USER_SUDOING}_id_rsa"
        cat "${TMP_DIR}/${host}_${USER_SUDOING}_id_rsa.pub" >> "${TMP_DIR}/authorized_keys"
    done

    docker build \
        --build-arg user="${USER_SUDOING}" \
        --build-arg ssh_user_key_priv="${TMP_DIR}/sms_${USER_SUDOING}_id_rsa" \
        --build-arg ssh_user_key_pub="${TMP_DIR}/sms_${USER_SUDOING}_id_rsa.pub" \
        --build-arg ssh_root_key_priv="${TMP_DIR}/sms_root_id_rsa" \
        --build-arg ssh_root_key_pub="${TMP_DIR}/sms_root_id_rsa.pub" \
        --build-arg authorized_keys="${TMP_DIR}/authorized_keys" \
        --build-arg sudo_nopasswd_file="${MMS_SUDOERS_NOPASSWD_FILE}" \
        -t "mms/sms-${image_suffix}:${MMS_VERSION}-not-installed" \
        -f "${dockerfile}" \
        "${docker_build_dir}"
    for host in "${HOST_ARRAY[@]}"; do
        docker build \
            --build-arg user="${USER_SUDOING}" \
            --build-arg ssh_user_key_priv="${TMP_DIR}/${host}_${USER_SUDOING}_id_rsa" \
            --build-arg ssh_user_key_pub="${TMP_DIR}/${host}_${USER_SUDOING}_id_rsa.pub" \
            --build-arg ssh_root_key_priv="${TMP_DIR}/${host}_root_id_rsa" \
            --build-arg ssh_root_key_pub="${TMP_DIR}/${host}_root_id_rsa.pub" \
            --build-arg authorized_keys="${TMP_DIR}/authorized_keys" \
            --build-arg sudo_nopasswd_file="${MMS_SUDOERS_NOPASSWD_FILE}" \
            -t "mms/${host}-${image_suffix}:${MMS_VERSION}-not-installed" \
            -f "${dockerfile}" \
            "${docker_build_dir}"
    done

    docker network create \
        --driver=bridge \
        --subnet=172.16.0.0/16 \
        --ip-range=172.16.0.0/17 \
        mms_test_net

    ip_last_byte="1"
    HOSTS_DOCKER_ARGS="--add-host=sms:172.16.${ip_third_byte}.${ip_last_byte} "
    for ((i=0 ; i<${#HOST_ARRAY[@]} ; i++)); do
        ip_last_byte=$[$i+2]
        host="${HOST_ARRAY[$i]}"
        HOSTS_DOCKER_ARGS="${HOSTS_DOCKER_ARGS}--add-host=${host}:172.16.${ip_third_byte}.${ip_last_byte} "
    done

    NVIDIA_DEVICES="$(ls /dev | grep nvidia)"
    NVIDIA_DEVICES_DOCKER_ARGS=""
    while read -r device; do
        NVIDIA_DEVICES_DOCKER_ARGS="${NVIDIA_DEVICES_DOCKER_ARGS}--device /dev/${device}:/dev/${device} "
    done <<< "$NVIDIA_DEVICES"

    # Start the built containers, pass through NVIDIA devices
    # for containers representing hosts that have a gpu.
    ip_last_byte="1"
    docker run -d \
        --name="sms-${image_suffix}-not-installed" \
        -h "sms" \
        --network="mms_test_net" \
        --ip="172.16.${ip_third_byte}.${ip_last_byte}" \
        --volume="$(pwd)/:/home/${USER_SUDOING}/repo" \
        ${HOSTS_DOCKER_ARGS} \
        ${NVIDIA_DEVICES_DOCKER_ARGS} \
        "mms/sms-${image_suffix}:${MMS_VERSION}-not-installed"
    for ((i=0 ; i<${#HOST_ARRAY[@]} ; i++)); do
        ip_last_byte=$[$i+2]
        host="${HOST_ARRAY[$i]}"
        nvidia_passthrough="${HOST_ARRAY_NVIDIA[$i]}"

        DOCKER_RUN_ARGS="-d --name=${host}-${image_suffix}-not-installed -h ${host} --network=mms_test_net --ip=172.16.${ip_third_byte}.${ip_last_byte} ${HOSTS_DOCKER_ARGS} "
        if $nvidia_passthrough; then
            DOCKER_RUN_ARGS="${DOCKER_RUN_ARGS}${NVIDIA_DEVICES_DOCKER_ARGS} "
        fi
        DOCKER_RUN_ARGS="${DOCKER_RUN_ARGS} mms/${host}-${image_suffix}:${MMS_VERSION}-not-installed"

        docker run ${DOCKER_RUN_ARGS}
    done

    mms_println "Give base contains a chance to boot, waiting 5 seconds..."
    sleep 5
    mms_println "Done."

    mms_ssh -tt "${USER_SUDOING}@172.16.${ip_third_byte}.1" "cd repo && sudo ./test_install.sh"

    # For each container: stop it, commit it (now that mms installation script has been applied), and remove it.
    docker stop "sms-${image_suffix}-not-installed"
    for host in "${HOST_ARRAY[@]}"; do
        docker stop "${host}-${image_suffix}-not-installed"
    done

    docker commit "sms-${image_suffix}-not-installed" "mms/sms-${image_suffix}:${MMS_VERSION}"
    for host in "${HOST_ARRAY[@]}"; do
        docker commit "${host}-${image_suffix}-not-installed" "mms/${host}-${image_suffix}:${MMS_VERSION}"
    done

    docker rm "sms-${image_suffix}-not-installed"
    for host in "${HOST_ARRAY[@]}"; do
        docker rm "${host}-${image_suffix}-not-installed"
    done
    
    # Finally, start the installed containers in earnest.
    ip_last_byte="1"
    docker run -d \
        ${custom_docker_run_flags} \
        --name="sms-${image_suffix}" \
        -h "sms" \
        --network="mms_test_net" \
        --ip="172.16.${ip_third_byte}.${ip_last_byte}" \
        ${HOSTS_DOCKER_ARGS} \
        ${NVIDIA_DEVICES_DOCKER_ARGS} \
        "mms/sms-${image_suffix}:${MMS_VERSION}"
    for ((i=0 ; i<${#HOST_ARRAY[@]} ; i++)); do
        ip_last_byte=$[$i+2]
        host="${HOST_ARRAY[$i]}"
        nvidia_passthrough="${HOST_ARRAY_NVIDIA[$i]}"

        DOCKER_RUN_ARGS="${custom_docker_run_flags} -d --name=${host}-${image_suffix} -h ${host} --network=mms_test_net --ip=172.16.${ip_third_byte}.${ip_last_byte} ${HOSTS_DOCKER_ARGS} "
        if $nvidia_passthrough; then
            DOCKER_RUN_ARGS="${DOCKER_RUN_ARGS}${NVIDIA_DEVICES_DOCKER_ARGS} "
        fi
        DOCKER_RUN_ARGS="${DOCKER_RUN_ARGS}mms/${host}-${image_suffix}:${MMS_VERSION}-not-installed"

        docker run ${DOCKER_RUN_ARGS}
    done

    rm -r "${TMP_DIR}"

    echo
    echo
    echo

    mms_println "Replica environment created and started. You can access the virtual software management server by running \"ssh 172.16.${ip_third_byte}.1\""

    echo
    echo
    echo

}

function stop_docker_replica_environment {
    mms_assert "$# -eq 1"
    local image_suffix="$1"

    docker stop "sms-${image_suffix}"
    docker stop "sms-${image_suffix}-not-installed"
    for host in "${HOST_ARRAY[@]}"; do
        docker stop "${host}-${image_suffix}"
        docker stop "${host}-${image_suffix}-not-installed"
    done
}

function start_docker_replica_environment {
    mms_assert "$# -eq 1"
    local image_suffix="$1"

    docker start "sms-${image_suffix}"
    for host in "${HOST_ARRAY[@]}"; do
        docker start "${host}-${image_suffix}"
    done
}

function destroy_docker_replica_environment {
    mms_assert "$# -eq 1"
    local image_suffix="$1"

    stop_docker_replica_environment "$image_suffix"

    docker rm "sms-${image_suffix}"
    docker rm "sms-${image_suffix}-not-installed"
    docker rmi "mms/sms-${image_suffix}:${MMS_VERSION}"
    docker rmi "mms/sms-${image_suffix}:${MMS_VERSION}-not-installed"
    for host in "${HOST_ARRAY[@]}"; do
        docker rm "${host}-${image_suffix}"
        docker rm "${host}-${image_suffix}-not-installed"
    done

    local min_tag="375"   # Increase this as commit numbers go on.
    for ((tag=${min_tag} ; tag<=${MMS_VERSION} ; tag++)); do
        docker rmi "mms/sms-${image_suffix}:${tag}"
        docker rmi "mms/sms-${image_suffix}:${tag}-not-installed"
    done
    for host in "${HOST_ARRAY[@]}"; do
        for ((tag=${min_tag} ; tag<=${MMS_VERSION} ; tag++)); do
            docker rmi "mms/${host}-${image_suffix}:${tag}"
            docker rmi "mms/${host}-${image_suffix}:${tag}-not-installed"
        done
    done
}

