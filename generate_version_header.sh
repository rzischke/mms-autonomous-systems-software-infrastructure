#!/bin/bash

# Add version header to use commit number, espeically
# for versioning the docker images in the test environment.

printf '#!/bin/bash\nMMS_VERSION='"$(git rev-list --count HEAD)\n" > joint/root/usr/local/bin/mms_version.sh

