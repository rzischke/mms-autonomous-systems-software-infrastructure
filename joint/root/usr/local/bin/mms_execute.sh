#!/bin/bash

[ -n "$MMS_EXECUTE_GUARD" ] && return || readonly MMS_EXECUTE_GUARD=1

source "mms_execute_variables.sh"

# Send emails by default. For a test environment, this can
# be set to zero before running email sending setup functions
# (for example, user creation).

[ -n "$MMS_SEND_EMAILS" ] || MMS_SEND_EMAILS=1

# Create admin group if it doesn't exit.
# Normally all this would be done by code in mms_account.sh,
# but that can't be sourced until the state directory
# has been created.
getent group "$ADMIN_GROUP" &> /dev/null
if [ $? -ne 0 ]; then
    addgroup "$ADMIN_GROUP"
fi

if [ ! -d "$MMS_MACHINE_STATE_DIR" ]; then
    # Normally all this would be done by code in mms_directories.sh,
    # but that can't be sourced until the state directory
    # has been created.
    mkdir --parents "$MMS_MACHINE_STATE_DIR"
    chown "root:${ADMIN_GROUP}" "$MMS_MACHINE_STATE_DIR"
    chmod 6770 "$MMS_MACHINE_STATE_DIR"
    setfacl --modify default:group::6
    setfacl --modify default:other::0
fi

source "mms_network.sh"
source "mms_account.sh"
source "mms_files.sh"
source "mms_directories.sh"
source "mms_install.sh"

function mms_execute_local {
    mms_assert "$# -eq 0"

    mms_apt_get_update

    # Install postfix and mailutils for emailing the new users.
    mms_create_dir /etc/postfix root:root 755
    mms_create_dir /etc/postfix/sasl root:root 755
    mms_create_file /etc/postfix/main.cf root:root 644
    mms_create_file /etc/postfix/sasl/sasl_passwd root:root 600
    mms_do_once "debconf-set-selections <<< \"postfix postfix/mailname string \$(hostname)\""
    mms_do_once "debconf-set-selections <<< \"postfix postfix/main_mailer_type select 'No configuration'\""
    mms_install_package "libsasl2-modules"
    mms_install_package "postfix"
    mms_do_once "postmap /etc/postfix/sasl/sasl_passwd"
    mms_create_file /etc/postfix/sasl/sasl_passwd.db root:root 600
    mms_install_package "mailutils"
    

    # Install packages (other than those installed here)
    if [ -d "$MMS_PACKAGE_DIR" ]; then
        mms_install_packages_from_dir "$MMS_PACKAGE_DIR"
    fi

    # Update users, and send new users an email with their
    # account details. This action can be omitted by setting
    # MMS_OMIT_USER_CREATION to true. This is handled in
    # mms_account.sh. This is needed for generating a test
    # or prototype environment where the administrator account
    # creating the environment clashes with a user in the file.
    mms_apply_users_and_group_file "$MMS_USER_FILE" "$MMS_GROUP_FILE"

    # Apply directories.
    if [ -f "$MMS_GLOBAL_DIR_FILE" ]; then
        mms_apply_dir_file "$MMS_GLOBAL_DIR_FILE" "$MMS_GROUP_FILE"
    fi
    if [ -f "$MMS_LOCAL_DIR_FILE" ]; then
        mms_apply_dir_file "$MMS_LOCAL_DIR_FILE" "$MMS_GROUP_FILE"
    fi

    # Apply files.
    if [ -f "$MMS_GLOBAL_FILE_FILE" ]; then
        mms_apply_file_file "$MMS_GLOBAL_FILE_FILE" "$MMS_GROUP_FILE"
    fi
    if [ -f "$MMS_LOCAL_DIR_FILE" ]; then
        mms_apply_file_file "$MMS_LOCAL_FILE_FILE" "$MMS_GROUP_FILE"
    fi
}

function mms_execute_remote {
    mms_assert "$# -eq 1"
    local host="$1"
    local ssh_dest="${USER_SUDOING}@${host}"
    mms_println "Executing source code on ${ssh_dest}..."
    mms_ssh -tt "$ssh_dest" "sudo /bin/bash -c \"source mms_execute.sh && mms_execute_local\""
    mms_println "${USER_SUDOING}@${host} source code execution complete."
}

function mms_execute_remote_all_hosts {
    mms_assert "$# -eq 0"
    mms_println "Executing source code on all hosts..."
    for host in "${HOST_ARRAY[@]}"; do
        mms_execute_remote "$host"
    done
    mms_println "Execution of source code on all hosts complete."
}

function mms_clean_local {
    mms_assert "$# -eq 0"
    mms_do "rm -r ${MMS_ASSETS_DIR}"
}

function mms_clean_remote {
    mms_assert "$# -eq 1"
    local host="$1"
    local ssh_dest="${USER_SUDOING}@${host}"
    mms_println "Cleaning up ${host}..."
    mms_ssh -tt "$ssh_dest" "sudo /bin/bash -c \"source mms_execute.sh && mms_clean_local\""
    mms_println "${host} cleaned."
}

function mms_clean_remote_all_hosts {
    mms_assert "$# -eq 0"
    mms_println "Cleaning up all hosts..."
    for host in "${HOST_ARRAY[@]}"; do
        mms_clean_remote "$host"
    done
    mms_println "Cleaning of all hosts complete."
}

